package org.belos.davdroid.settings;

interface ISettingsObserver {

    void onSettingsChanged();

}
