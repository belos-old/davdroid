/**
 * Automatically generated file. DO NOT MODIFY
 */
package org.belos.davdroid;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "org.belos.davdroid";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "standard";
  public static final int VERSION_CODE = 189;
  public static final String VERSION_NAME = "1.9.3-ose";
  // Fields from product flavor: standard
  public static final boolean customCerts = true;
  public static final boolean customCertsUI = true;
  // Fields from default config.
  public static final long buildTime = 1514012113577L;
}
