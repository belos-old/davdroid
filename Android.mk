LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
 
# Name of the APK to build
LOCAL_MODULE := Sync
LOCAL_MODULE_TAGS := optional
LOCAL_PACKAGE_NAME := Sync

sync_root	:= $(LOCAL_PATH)
sync_dir	:= app
sync_out	:= $(PWD)/$(OUT_DIR)/target/common/obj/APPS/$(LOCAL_MODULE)_intermediates
sync_build	:= $(sync_root)/$(sync_dir)/build
sync_apk	:= build/outputs/apk/standard/release/$(sync_dir)-standard-release-unsigned.apk

$(sync_root)/$(sync_dir)/$(sync_apk):
	#cd $(sync_root) && sed -i -e 's/gradle-4.0.1/gradle-4.0/' gradle/wrapper/gradle-wrapper.properties
	cd $(sync_root) && git submodule init && git submodule update --remote
	cd $(sync_root)/$(sync_dir) && gradle assembleRelease

# Build all java files in the java subdirectory
LOCAL_CERTIFICATE := platform
LOCAL_SRC_FILES := $(sync_dir)/$(sync_apk)
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
 
# Tell it to build an APK
include $(BUILD_PREBUILT)
